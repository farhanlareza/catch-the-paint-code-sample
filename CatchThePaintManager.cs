﻿using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System.Collections.Generic;

public class CatchThePaintManager : MonoBehaviour
{
    public static CatchThePaintManager instance;
    
    [SerializeField] GameObject countdownPanel;
    [SerializeField] TextMeshProUGUI countdownTitleText;
    [SerializeField] Image countdownTitleImage;
    [SerializeField] TextMeshProUGUI countdownText;
    [SerializeField] float maxCountdownTime;
    float countdownTime;
    int latestCountdownTime;
    bool isCountingDown;

    [SerializeField] TextMeshProUGUI instructionText;
    [SerializeField] Image instructionImage;
    [SerializeField] TextMeshProUGUI scoreText;
    [SerializeField] TextMeshProUGUI missedText;

    [SerializeField] GameObject progressbar;
    [SerializeField] Image progressbarFill;
    [SerializeField] float progressbarAnimateMaxTime;
    float progressBarIncrementValue;
    float progressbarAnimateTimeLeft;
    bool isAnimatingProgressbar;

    [SerializeField] GameObject paintPrefab;
    float initialSpawnTime;
    float limitSpawnTime;
    float decceleratorSpawnTime;
    float spawnTime;
    bool isSpawning;

    [SerializeField] ColorChangeAnimator colorchangeAnim;
    float initialColorchangeTime;
    float colorchangeTime;

    [SerializeField] BucketController bucketController;
    [SerializeField] GameObject succeedPanel;
    [SerializeField] TextMeshProUGUI succeedScoreText;
    [SerializeField] GameObject failedPanel;
    
    #region UNITY CALLBACKS
    private void Awake()
    {
        if (instance == null) instance = this;
        else if (instance != null && instance != this) {Destroy(gameObject); return; }

        succeedPanel.SetActive(false);
        failedPanel.SetActive(false);
    }

    private void Update()
    {
        CountingDown();
        UpdateProgressbar();
        if (!isSpawning) return;

        SpawnPaint();
        DeccelerateInitialSpawnTime();
        ChangeTargetColor();
    }
    #endregion

    #region PRIVATE FUNCTION

    void CountingDown()
    {
        if (!isCountingDown) return;

        float decresedCountdownTIme = countdownTime -= Time.deltaTime;
        if (decresedCountdownTIme <= 0)
        {
            spawnTime = 0;
            isSpawning = true;
            isCountingDown = false;
        }
        else if (decresedCountdownTIme <= 1 && countdownPanel.activeSelf)
        {
            countdownPanel.SetActive(false);
            bucketController.SetToMoving();
            if (latestCountdownTime != (int)decresedCountdownTIme)
            {
                latestCountdownTime = (int)decresedCountdownTIme;
                AudioManager.instance.PlayCountdownAudio(true);
            }
        }
        else
        {
            countdownText.SetText(((int)decresedCountdownTIme).ToString());
            if (latestCountdownTime != (int)decresedCountdownTIme)
            {
                latestCountdownTime = (int)decresedCountdownTIme;
                AudioManager.instance.PlayCountdownAudio(false);
            }
        }
    }

    void SpawnPaint()
    {
        if (spawnTime <= 0)
        {
            float upperBound = GameManager.instance.GetUpperBound();
            upperBound = upperBound > 0 ? upperBound + (upperBound * 0.4f) : upperBound - (upperBound * 0.4f);

            Vector3 initialPos = new Vector3(GameManager.instance.GetHorizontalViewportToWorldPosition(Random.Range(0.1f, 0.9f)), upperBound, 0);
            GameObject collectableItem = Instantiate(paintPrefab);
            collectableItem.transform.position = initialPos;
            collectableItem.name = Keyword.PAINT_ITEM_NAME;
            spawnTime = initialSpawnTime;
        }
        else spawnTime -= Time.deltaTime;
    }

    void DeccelerateInitialSpawnTime()
    {
        if (initialSpawnTime <= limitSpawnTime) return;

        float updatedInitialSpawnTIme = initialSpawnTime + decceleratorSpawnTime * Time.deltaTime;
        initialSpawnTime = updatedInitialSpawnTIme < limitSpawnTime ? limitSpawnTime : updatedInitialSpawnTIme;
    }

    void ChangeTargetColor()
    {
        if (GameManager.instance.GetGameplayData().mode != Keyword.PAINT_MODE_COLORCHANGE) return;

        if (colorchangeTime <= 0)
        {
            colorchangeAnim.SetToDisplay();
            var newColorList = new List<AvitexColor>(Keyword.PAINT_COLORS);
            newColorList.RemoveAt(GameManager.instance.GetGameplayData().pointOfInterest);
            var newColor = newColorList[Random.Range(0, newColorList.Count)];

            int newPov = Keyword.PAINT_COLORS.IndexOf(newColor);
            GameManager.instance.GetGameplayData().ChangePointOfInterest(newPov);
            instructionImage.color = Keyword.PAINT_COLORS[newPov].color;
            colorchangeTime = initialColorchangeTime;
            AudioManager.instance.PlayColorChangeAudio();
        }
        else colorchangeTime -= Time.deltaTime;
    }

    void UpdateProgressbar()
    {
        if (isAnimatingProgressbar && progressbarAnimateTimeLeft <= 0)
        {
            isAnimatingProgressbar = false;
            progressbarAnimateTimeLeft = 0;
            progressbarFill.fillAmount = GameManager.instance.GetGameplayData().progressPercentage;
        }
        else if (isAnimatingProgressbar)
        {
            float timerDivisor = Mathf.Clamp(progressbarAnimateMaxTime, 0.1f, 2f);
            float timeAfterDecresed = progressbarAnimateTimeLeft - (Time.deltaTime / timerDivisor);

            if (timeAfterDecresed <= 0)
            {
                float incrementValue = progressbarAnimateTimeLeft * progressBarIncrementValue;
                progressbarFill.fillAmount += incrementValue;
                progressbarAnimateTimeLeft = 0;
            }
            else
            {
                float incrementValue = (progressbarAnimateTimeLeft- timeAfterDecresed) * progressBarIncrementValue;
                progressbarFill.fillAmount += incrementValue;
                progressbarAnimateTimeLeft= timeAfterDecresed;
            }
        }
    }

    void UpdateScoreText()
    {
        scoreText.SetText(GameManager.instance.GetGameplayData().scoreSentence);
        succeedScoreText.SetText(GameManager.instance.GetGameplayData().scoreSentence);
    }

    void UpdateMissedText()
    {
        missedText.SetText(GameManager.instance.GetGameplayData().missedSentence);
    }
    #endregion

    #region PUBLIC FUNCTION
    public void SetupGameplay()
    {
        HighscoreManager.instance.SetSmallestHighscore();

        instructionText.SetText(GameManager.instance.GetGameplayData().shortInstructionSentence);
        instructionImage.color = Keyword.PAINT_COLORS[GameManager.instance.GetGameplayData().pointOfInterest].color;
        UpdateScoreText();
        UpdateMissedText();
        if (GameManager.instance.GetGameplayData().mode == Keyword.PAINT_MODE_NORMAL) progressbarFill.fillAmount = 0;
        else progressbar.SetActive(false); 

        initialSpawnTime = GameManager.instance.GetSpeedCustomizeData().initialSpawnTime;
        limitSpawnTime = GameManager.instance.GetSpeedCustomizeData().limitSpawnTime;
        decceleratorSpawnTime = GameManager.instance.GetSpeedCustomizeData().deceleratorSpawnSpeed;
        initialColorchangeTime = GameManager.instance.GetSpeedCustomizeData().initialColorchangeTime;
        colorchangeTime = initialColorchangeTime;

        countdownTitleText.SetText(GameManager.instance.GetGameplayData().fullInstructionSentence);
        countdownTitleImage.color = Keyword.PAINT_COLORS[GameManager.instance.GetGameplayData().pointOfInterest].color;
        countdownText.SetText((maxCountdownTime - 1).ToString());
        countdownTime = maxCountdownTime;
        latestCountdownTime = (int)(maxCountdownTime - 1);
    }

    public void StartCountingDown()
    {
        isCountingDown = true;
    }

    public void IncreaseProgressbar()
    {
        if (GameManager.instance.GetGameplayData().mode != Keyword.PAINT_MODE_NORMAL) return;

        progressBarIncrementValue = GameManager.instance.GetGameplayData().progressPercentageIncrement;
        progressbarAnimateTimeLeft += 1;
        isAnimatingProgressbar = true;
    }

    public void SetToCompleted(bool isSucceed)
    {
        if (isSucceed){succeedPanel.SetActive(true); AudioManager.instance.PlaySucceedAudio();}
        else { failedPanel.SetActive(true); AudioManager.instance.PlayFailedAudio(); }
        
        isSpawning = false;
        bucketController.SetToStatic();
        foreach (PaintBehaviour paint in FindObjectsOfType<PaintBehaviour>()) Destroy(paint.gameObject);
    }

    public void UpdatePlayerDataVisual()
    {
        UpdateScoreText();
        UpdateMissedText();

        if (GameManager.instance.GetGameplayData().IsSucceed()) SetToCompleted(true);
        else if (GameManager.instance.GetGameplayData().IsFailed()) SetToCompleted(false);
    }
    #endregion

    #region BUTTON & Listener
    public void ProceedSucceedButton()
    {
        if (HighscoreManager.instance.smallestHighscore < GameManager.instance.GetGameplayData().score)
        {
            HighscoreManager.instance.OpenNewHighscorePanel();
            succeedPanel.SetActive(false);
        }
        else GameManager.instance.LoadScene(Keyword.SCENE_MAIN_MENU);
    }

    public void TryAgainButton(Button button)
    {
        button.interactable = false;
        GameManager.instance.LoadScene(Keyword.SCENE_CATCH_THE_PAINT);
    }

    public void HomeButton()
    {
        GameManager.instance.LoadScene(Keyword.SCENE_MAIN_MENU);
    }
    #endregion
}