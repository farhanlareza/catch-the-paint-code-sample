﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelMenuManager : MonoBehaviour
{
    #region BUTTON
    public void BackButton()
    {
        GameManager.instance.LoadScene(Keyword.SCENE_MAIN_MENU);
    }
    #endregion
}
