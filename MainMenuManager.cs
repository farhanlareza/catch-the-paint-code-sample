﻿using UnityEngine;

public class MainMenuManager : MonoBehaviour
{
    [SerializeField] GameObject modePanel;

    #region UNITY CALLBACKS
    private void Awake()
    {
        modePanel.SetActive(false);
    }
    #endregion

    #region PRIVATE FUNCTION
    void OpenModePanel()
    {
        modePanel.SetActive(true);
    }
    void CloseModePanel()
    {
        modePanel.SetActive(false);
    }
    #endregion

    #region BUTTON
    public void PlayButton()
    {
        OpenModePanel();
        AudioManager.instance.PlayButtonAudio();
    }

    public void BackButton()
    {
        CloseModePanel();
        AudioManager.instance.PlayButtonAudio();
    }
    
    public void EnterModeButton(int modeIndex)
    {
        GameManager.instance.NewGameCatchThePaint(modeIndex);
        AudioManager.instance.PlayButtonAudio();
    }

    public void ShowHighscoreButton()
    {
        HighscoreManager.instance.OpenHighscorePanel();
        AudioManager.instance.PlayButtonAudio();
    }
    #endregion
}
