﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    [SerializeField] Image transitionPanel;
    [SerializeField] float transitionTime;
    int transitionState;
    string sceneToLoad;

    Camera othoCam;
    static GameplayData gameplayData;
    static SpeedCustomizeData speedCustomizeData;

    #region UNITY CALLBACKS
    private void Awake()
    {
        if (instance == null) instance = this;
        else if (instance != null && instance != this) { Destroy(gameObject); return; }

        transitionPanel.gameObject.SetActive(false);
        gameplayData = null;
        speedCustomizeData = SpeedCustomizeDataSerializer.Load();
        DontDestroyOnLoad(gameObject);
    }

    private void Update()
    {
        UpdateTransition();
    }

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (scene.name == Keyword.SCENE_CATCH_THE_PAINT){ gameplayData.ResetAllData(); CatchThePaintManager.instance.SetupGameplay();}
        else if (scene.name == Keyword.SCENE_TRANSITION_LAYER) SceneManager.LoadScene(sceneToLoad);

        if (transitionState > 0) transitionState -= 1;

        AudioManager.instance.PlayBacksoundAudio();
        HighscoreManager.instance.CloseHighscorePanel();
    }
    #endregion

    #region PRIVATE FUNCTION
    void UpdateTransition()
    {
        if (transitionState <= 0) return;

        if (transitionState == 3)
        {
            if (transitionPanel.color.a >= 1)
            {
                SceneManager.LoadScene(Keyword.SCENE_TRANSITION_LAYER);
            }
            else
            {
                Color updatedPanelColor = transitionPanel.color;
                updatedPanelColor.a += Time.deltaTime / transitionTime;
                transitionPanel.color = updatedPanelColor;
            }
        }
        else if (transitionState == 2)
        {
            Debug.Log("ERROR");
        }
        else if (transitionState == 1) 
        {
            if (transitionPanel.color.a <= 0)
            {
                transitionPanel.gameObject.SetActive(false);
                transitionState = 0;
                if (sceneToLoad == Keyword.SCENE_CATCH_THE_PAINT) CatchThePaintManager.instance.StartCountingDown();
            }
            else
            {
                Color updatedPanelColor = transitionPanel.color;
                updatedPanelColor.a -= Time.deltaTime / transitionTime;
                transitionPanel.color = updatedPanelColor;
            }
        }
    }
    #endregion

    #region PUBLIC FUNCTION
    public void NewGameCatchThePaint(int gameMode)
    {
        gameplayData = new CatchThePaintData(gameMode);
        LoadScene(Keyword.SCENE_CATCH_THE_PAINT);
    }

    public GameplayData GetGameplayData()
    {
        return gameplayData;
    }

    public SpeedCustomizeData GetSpeedCustomizeData()
    {
        return speedCustomizeData;
    }

    public void LoadScene(string sceneName)
    {
        if (transitionState > 0) return;

        transitionPanel.gameObject.SetActive(true);
        sceneToLoad = sceneName;
        transitionState = 3;
        AudioManager.instance.PlayButtonAudio();
    }

    public float GetLeftBound()
    {
        if (othoCam == null) othoCam = GameObject.Find(Keyword.CAMERA_OTHOGONAL).GetComponent<Camera>();
        return othoCam.ViewportToWorldPoint(Vector3.zero).x;
    }

    public float GetRightBound()
    {
        if (othoCam == null) othoCam = GameObject.Find(Keyword.CAMERA_OTHOGONAL).GetComponent<Camera>();
        return othoCam.ViewportToWorldPoint(Vector3.right).x;
    }

    public float GetLowerBound()
    {
        if (othoCam == null) othoCam = GameObject.Find(Keyword.CAMERA_OTHOGONAL).GetComponent<Camera>();
        return othoCam.ViewportToWorldPoint(Vector3.zero).y;
    }

    public float GetUpperBound()
    {
        if (othoCam == null) othoCam = GameObject.Find(Keyword.CAMERA_OTHOGONAL).GetComponent<Camera>();
        return othoCam.ViewportToWorldPoint(Vector3.up).y;
    }

    public float GetHorizontalViewportToWorldPosition(float viewportPos)
    {
        if (othoCam == null) othoCam = GameObject.Find(Keyword.CAMERA_OTHOGONAL).GetComponent<Camera>();
        return othoCam.ViewportToWorldPoint(Vector3.one * viewportPos).x;
    }
    #endregion
}

#region DATA CLASS
public abstract class GameplayData
{
    public int mode { get; protected set; }
    public int score { get; protected set; }
    public int missed { get; protected set; }
    public int maxMissed { get; protected set; }
    public float progressPercentage { get; protected set; }
    public float progressPercentageIncrement { get; protected set; }
    public int pointOfInterest { get; protected set; }
    public string shortInstructionSentence { get; protected set; }
    public string fullInstructionSentence { get; protected set; }
    public string scoreSentence { get; protected set; }
    public string missedSentence { get; protected set; }

    public void IncreaseScore()
    {
        progressPercentage += progressPercentageIncrement;
        score += 10;
    }

    public void TakePinalty()
    {
        if (score >= 3) score -= 3;
        missed += 1;
    }

    public void UpdateSentence()
    {
        scoreSentence = "Score: " + score;
        missedSentence = "Missed: " + missed + "/" + maxMissed;
    }
    
    public bool IsSucceed()
    {
        return (progressPercentage >= 1 && mode == Keyword.PAINT_MODE_NORMAL)
            || (missed >= maxMissed && (mode == Keyword.PAINT_MODE_ENDLESS))
            || (missed >= maxMissed && (mode == Keyword.PAINT_MODE_COLORCHANGE));
    }

    public bool IsFailed()
    {
        return missed >= maxMissed && mode == Keyword.PAINT_MODE_NORMAL;
    }

    public abstract void ChangePointOfInterest(int newPov);
    public abstract bool SubmitAndUpdate(int pov);
    public abstract void ResetAllData();
}

public class CatchThePaintData : GameplayData
{
    public override void ChangePointOfInterest(int newPov)
    {
        pointOfInterest = newPov;
    }

    public override bool SubmitAndUpdate(int pov)
    {
        bool isScoring = pov == pointOfInterest;
        if (isScoring) IncreaseScore();
        else TakePinalty();
        UpdateSentence();

        return isScoring;
    }

    public override void ResetAllData()
    {
        score = 0;
        missed = 0;
        maxMissed = 10;
        progressPercentage = 0;
        progressPercentageIncrement = 0.1f;
        pointOfInterest = UnityEngine.Random.Range(0,Keyword.PAINT_COLORS.Count);
        fullInstructionSentence = "Collect paint droplets that match this color";
        shortInstructionSentence = "Collect";
        scoreSentence = "Score: " + score;
        missedSentence = "Missed: " + missed + "/" + maxMissed;
    }

    public CatchThePaintData(int targetMode)
    {
        mode = targetMode;
        ResetAllData();
    }
}

public class AvitexColor
{
    public string colorName { get; protected set; }
    public Color color { get; protected set; }
}

public class AvitexRed : AvitexColor
{
    public AvitexRed()
    {
        colorName = "Red";
        color = new Color(0.9176f, 0.1960f, 0.2353f);
    }
}

public class AvitexYellow : AvitexColor
{
    public AvitexYellow()
    {
        colorName = "Yellow";
        color = new Color(1f, 0.9215f, 0.3411f);
    }
}

public class AvitexBlue : AvitexColor
{
    public AvitexBlue()
    {
        colorName = "Blue";
        color = new Color(0f, 0.5960f, 0.8627f);
    }
}

public class AvitexGreen : AvitexColor
{
    public AvitexGreen()
    {
        colorName = "Green";
        color = new Color(0.3529f, 0.7725f, 0.3096f);
    }
}

public class AvitexPurple : AvitexColor
{
    public AvitexPurple()
    {
        colorName = "Purple";
        color = new Color(0.4784f, 0.0352f, 0.9801f);
    }
}

public class SpeedCustomizeData
{
    [XmlAttribute] public float initialSpawnTime;
    [XmlAttribute] public float limitSpawnTime;
    [XmlAttribute] public float deceleratorSpawnSpeed;
    [XmlAttribute] public float paintDropSpeed;
    [XmlAttribute] public float paintCorrectColorRate;
    [XmlAttribute] public float bucketMinimumInputValue;
    [XmlAttribute] public float bucketMovementSpeed;
    [XmlAttribute] public float initialColorchangeTime;

    public SpeedCustomizeData()
    {
        initialSpawnTime = 1f;
        limitSpawnTime = 0.4f;
        deceleratorSpawnSpeed = -0.01f;
        paintDropSpeed = 5f;
        paintCorrectColorRate = 0.4f;
        bucketMinimumInputValue = 0.05f;
        bucketMovementSpeed = 15f;
        initialColorchangeTime = 10f;
    }
}

public class SpeedCustomizeDataSerializer
{
    public static SpeedCustomizeData Load()
    {
        string directory = Application.persistentDataPath + "/" + Keyword.PAINT_DIRECTORY;
        string filename = directory + "/" + Keyword.PAINT_SPEED_FILENAME;

        if (File.Exists(filename))
        {
            try
            {
                var serializer = new XmlSerializer(typeof(SpeedCustomizeData));
                var stream = new FileStream(filename, FileMode.Open);
                var container =  serializer.Deserialize(stream) as SpeedCustomizeData;
                stream.Close();
                return container;
                
            }
            catch (Exception e)
            {
                Debug.Log(e.Message);
            }
        }

        Debug.Log("Create new xml data");
        var newContainer = new SpeedCustomizeData();
        Save(newContainer);
        return newContainer;
    }

      public static void Save(SpeedCustomizeData data) 
    {
        string directory = Application.persistentDataPath + "/" + Keyword.PAINT_DIRECTORY;
        string filename = directory + "/" + Keyword.PAINT_SPEED_FILENAME;

        if (!Directory.Exists(directory)) Directory.CreateDirectory(directory);

        var serializer = new XmlSerializer(typeof(SpeedCustomizeData));
        var stream = new FileStream(filename, FileMode.Create);
        serializer.Serialize(stream, data);
    }

    public static void Delete()
    {
        string directory = Application.persistentDataPath + "/" + Keyword.PAINT_DIRECTORY;
        string filename = directory + "/" + Keyword.PAINT_SPEED_FILENAME;

        if(Directory.Exists(directory)) File.Delete(filename);
    }
}

#endregion