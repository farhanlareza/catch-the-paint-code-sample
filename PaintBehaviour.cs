﻿using UnityEngine;

public class PaintBehaviour : MonoBehaviour
{
    BucketController bucketController;
    [SerializeField] GameObject[] particlePrefabs;
    [SerializeField] Renderer meshRenderer;
    float dropSpeed;
    float correctColorRate;
    AvitexColor avitexColor;
    int colorIndex;
    

    #region UNITY CALLBACKS
    void Awake()
    {
        correctColorRate = GameManager.instance.GetSpeedCustomizeData().paintCorrectColorRate;
        dropSpeed = GameManager.instance.GetSpeedCustomizeData().paintDropSpeed;

        colorIndex = Random.Range(0f, 1f) <= correctColorRate ?
            GameManager.instance.GetGameplayData().pointOfInterest : Random.Range(0, Keyword.PAINT_COLORS.Count);
        avitexColor = Keyword.PAINT_COLORS[colorIndex];
        meshRenderer.material.color = avitexColor.color;
        bucketController = GameObject.Find(Keyword.PAINT_BUCKET_NAME).GetComponent<BucketController>();
    }

    void Update()
    {
        transform.position += Vector3.down * dropSpeed * Time.deltaTime;
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == Keyword.PAINT_BUCKET_NAME && bucketController.isMoving)
        {
            bool isScoring = GameManager.instance.GetGameplayData().SubmitAndUpdate(colorIndex);
            if (isScoring)
            {  
                CatchThePaintManager.instance.IncreaseProgressbar();
                InstatiateParticle(1, bucketController.transform.position, true);
                AudioManager.instance.PlayScoringAudio();
            }
            else
            {
                InstatiateParticle(2, bucketController.transform.position, false);
                AudioManager.instance.PlayPenaltyAudio();
            }
            CatchThePaintManager.instance.UpdatePlayerDataVisual();
        }
        else
        {
            if (colorIndex == GameManager.instance.GetGameplayData().pointOfInterest)
            {
                GameManager.instance.GetGameplayData().SubmitAndUpdate(-1);
                CatchThePaintManager.instance.UpdatePlayerDataVisual();
            }
            InstatiateParticle(0, transform.position, true);
            AudioManager.instance.PlayPaintdropAudio();
        }

        Destroy(gameObject);
    }
    #endregion

    #region PRIVATE FUNCTION
    void InstatiateParticle(int index, Vector3 position, bool isColorize)
    {
        GameObject particle = Instantiate(particlePrefabs[index]);
        particle.transform.position = position;
        if (isColorize)
        {
            var psMain = particle.GetComponent<ParticleSystem>().main;
            psMain.startColor = avitexColor.color;
        }
    }
    #endregion
}
