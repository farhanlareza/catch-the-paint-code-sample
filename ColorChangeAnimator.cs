﻿
using UnityEngine;

public class ColorChangeAnimator : MonoBehaviour
{
    [SerializeField] float distanceTolerance;
    [SerializeField] float[] xIdealPositions;
    float xDesiredPosition;

    [SerializeField]float animatingTime;
    bool isAnimating;

    #region UNITY CALLBACKS
    private void Update()
    {
        if (!isAnimating) return;

        Animate();
    }
    #endregion

    #region PRIVATE FUNCTION
    void Animate()
    {
        if (!isAnimating) return;

        float distance = xDesiredPosition - transform.localPosition.x;
        float xDesiredMovement = Mathf.Abs(distance) > distanceTolerance ? distance * Time.deltaTime / animatingTime : 0;

        if (xDesiredMovement != 0)
        {
            transform.localPosition += Vector3.right * xDesiredMovement;
        }
        else if(xDesiredPosition == xIdealPositions[1])
        {
            transform.localPosition = new Vector3(xDesiredPosition, transform.localPosition.y);
            xDesiredPosition = xIdealPositions[0];
        }
        else if(xDesiredPosition == xIdealPositions[0])
        {
            transform.localPosition = new Vector3(xDesiredPosition, transform.localPosition.y);
            isAnimating = false;
        }
    }
    #endregion

    #region PUBLIC FUNCTION
    public void SetToDisplay()
    {
        xDesiredPosition = xIdealPositions[1];
        isAnimating = true;
    }
    #endregion
}
