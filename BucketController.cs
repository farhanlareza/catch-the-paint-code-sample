using UnityEngine;

public class BucketController : MonoBehaviour
{
    float movementSpeed;
    float minimumInputValue;
    public bool isMoving { get; private set; }

    #region UNITY CALLBACKS
    void Update()
    {
        if (!isMoving) return;

        float horizontalInputValue = Input.GetAxis(Keyword.INPUT_HORIZONTAL);
        if (horizontalInputValue > 0 && (horizontalInputValue < minimumInputValue
            || transform.position.x >= GameManager.instance.GetRightBound())) horizontalInputValue = 0;
        else if (horizontalInputValue < 0 && (horizontalInputValue > minimumInputValue * -1
            || transform.position.x <= GameManager.instance.GetLeftBound())) horizontalInputValue = 0;

        transform.position += Vector3.right * horizontalInputValue * movementSpeed * Time.deltaTime;
        AdjustOutOfBoundPosition();
    }
    #endregion

    #region PRIVATE FUNCTION
    void AdjustOutOfBoundPosition()
    {
        if(transform.position.x < GameManager.instance.GetLeftBound())
            transform.position = new Vector3(GameManager.instance.GetLeftBound(), transform.position.y, transform.position.z);
        
        if(transform.position.x > GameManager.instance.GetRightBound())
            transform.position = new Vector3(GameManager.instance.GetRightBound(), transform.position.y, transform.position.z);
    }
    #endregion

    #region PUBLIC FUNCTION
    public void SetToStatic()
    {
        isMoving = false;
    }

    public void SetToMoving()
    {
        movementSpeed = GameManager.instance.GetSpeedCustomizeData().bucketMovementSpeed;
        minimumInputValue = GameManager.instance.GetSpeedCustomizeData().bucketMinimumInputValue;
        isMoving = true;
    }
    #endregion
}
