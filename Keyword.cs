﻿using System.Collections.Generic;
    
public class Keyword
{
    #region GENERAL
    public static string INPUT_HORIZONTAL = "Horizontal";
    public static string INSTRUCTION_NAME = "instruction.text";
    public static string SCENE_CATCH_THE_PAINT = "Catch The Paint";
    public static string SCENE_MAIN_MENU = "Main Menu";
    public static string SCENE_LEVEL_MENU = "Level Menu";
    public static string SCENE_TRANSITION_LAYER = "Transition Layer";
    public static string HIGHSCORE_DIRECTORY = "Highscore Data";
    public static string HIGHSCORE_FILENAME = "highscore_data_mode_";
    public static string HIGHSCORE_INPUTNAME_INSTRUCTION = "Please Enter Your Name.";
    public static string HIGHSCORE_INPUTNAME_INVALID_EMPTY = "Name cannot be empty.";
    public static string HIGHSCORE_INPUTNAME_INVALID_NONALPHABET = "Name must contain \nat least 1 alphabetical letter.";
    public static string HIGHSCORE_INPUTNAME_INVALID_SPACEATFIRST = "First character cannot be \na space letter.";
    public static string HIGHSCORE_INPUTNAME_INVALID_TOOSHORT = "Name cannot be less than 3 character.";
    public static string HIGHSCORE_INPUTNAME_INVALID_NULL = "Name cannot be null";
    public static string CAMERA_OTHOGONAL = "camera.othogonal";
    public static string NULL = "null";
    public static string TAG_COLOR_ORANGE = "<color=#FF6000>";
    public static string TAG_COLOR_BLACK = "<color=#000000>";
    #endregion

    #region CATCH THE PAINT
    public static string PAINT_DIRECTORY = "Paint Speed Data";
    public static string PAINT_SPEED_FILENAME = "paint_speed";
    public static int PAINT_MODE_NORMAL = 0;
    public static int PAINT_MODE_ENDLESS = 1;
    public static int PAINT_MODE_COLORCHANGE = 2;
    public static string PAINT_ITEM_NAME = "paint.item";
    public static string PAINT_BUCKET_NAME = "bucket.item";
    public static string PAINT_PROCEED_WIN_BUTTON = "paint.proceedwin.button";
    public static string PAINT_ANIM_COLORCHANGE = "color_change";
    public static List<AvitexColor> PAINT_COLORS = new List<AvitexColor> { new AvitexYellow(), new AvitexBlue(), new AvitexPurple(), new AvitexGreen(), new AvitexRed()};
    #endregion
}
