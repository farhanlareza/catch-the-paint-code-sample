﻿using UnityEngine.UI;
using UnityEngine;
using TMPro;
using UnityEngine.EventSystems;
public class VirtualKeyboardManager : MonoBehaviour, IDragHandler
{
    public static VirtualKeyboardManager instance;

    [SerializeField] Image virtualKeyboard;
    [SerializeField] Image shiftKeyImage;
    [SerializeField] Sprite[] shiftKeySprites;
    [SerializeField] TextMeshProUGUI[] alphabetKeyText;
    Vector3 initialPosition;
    TMP_InputField inputField;
    bool isCapital;

    #region UNITY CALLBACKS
    private void Awake()
    {
        if (instance == null) instance = this;
        else if(instance !=null && instance !=this) { Destroy(gameObject); return; }

        initialPosition = transform.localPosition;
        CloseKeyboard();
    }
    #endregion

    #region PRIVATE FUNCTION
    void UpdateKeyboardVisual()
    {
        for(int i=0; i < alphabetKeyText.Length; i++) alphabetKeyText[i].text = isCapital ?
                alphabetKeyText[i].text.ToUpper() : alphabetKeyText[i].text.ToLower();
        shiftKeyImage.sprite = isCapital ? shiftKeySprites[1] : shiftKeySprites[0];
    }
    #endregion

    #region PUBLIC FUNCTION
    public void OpenKeyboard(TMP_InputField targetInput)
    {
        inputField = targetInput;
        transform.localPosition= initialPosition;
        isCapital = false;
        UpdateKeyboardVisual();
        virtualKeyboard.gameObject.SetActive(true);
    }

    public void CloseKeyboard()
    {
        virtualKeyboard.gameObject.SetActive(false);
    }
    #endregion

    #region BUTTON & LISTENER
    public void InsertButton(TextMeshProUGUI text)
    {
        if (inputField.text.Length >= inputField.characterLimit) return; 

        inputField.text += text.text;
    }

    public void BackSpaceButton()
    {
        if (inputField.text.Length > 0) inputField.text = inputField.text.Remove(inputField.text.Length - 1);
    }

    public void ShiftButton()
    {
        isCapital = !isCapital;
        UpdateKeyboardVisual();
    }

    public void CloseButton()
    {
        CloseKeyboard();
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = eventData.position;
    }
    #endregion
}
