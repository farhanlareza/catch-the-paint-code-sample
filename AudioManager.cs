﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;
    [SerializeField] AudioSource menuBacksoundAudio;
    [SerializeField] AudioSource ingameBacksoundAudio;
    [SerializeField] AudioSource penaltyAudio;
    [SerializeField] AudioSource scoringAudio;
    [SerializeField] AudioSource paintdropAudio;
    [SerializeField] AudioSource colorChangeAudio;
    [SerializeField] AudioSource buttonAudio;
    [SerializeField] AudioSource countdownAudio;
    [SerializeField] AudioSource succeedAudio;
    [SerializeField] AudioSource failedAudio;

    [SerializeField] AudioClip[] countdownClip;

    #region UNITY CALLBACKS
    private void Awake()
    {
        if (instance == null) instance = this;
        else if(instance!= null && instance != this) { Destroy(gameObject);return; }
    }
    #endregion

    #region PUBLIC FUNCTION
    public void PlayBacksoundAudio()
    {
        string sceneName = SceneManager.GetActiveScene().name;

        if (sceneName == Keyword.SCENE_MAIN_MENU)
        {
            ingameBacksoundAudio.Stop();
            menuBacksoundAudio.Play();
        }
        else if(sceneName == Keyword.SCENE_CATCH_THE_PAINT)
        {
            menuBacksoundAudio.Stop();
            ingameBacksoundAudio.Play();
        }
    }

    public void PlayCountdownAudio(bool isFinal)
    {
        if (!isFinal) countdownAudio.clip = countdownClip[0];
        else countdownAudio.clip = countdownClip[1];
        countdownAudio.Play();
    }

    public void PlayPenaltyAudio()
    {
        penaltyAudio.Play();
    }

    public void PlayScoringAudio()
    {
        scoringAudio.Play();
    }

    public void PlayButtonAudio()
    {
        buttonAudio.Play();
    }

    public void PlayPaintdropAudio()
    {
        paintdropAudio.Play();
    }

    public void PlayColorChangeAudio()
    {
        colorChangeAudio.Play();
    }

    public void PlaySucceedAudio()
    {
        succeedAudio.Play();
    }

    public void PlayFailedAudio()
    {
        failedAudio.Play();
    }
    #endregion
}
