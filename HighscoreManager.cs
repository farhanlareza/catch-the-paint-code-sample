﻿using System;
using System.IO;
using UnityEngine;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
using System.Text.RegularExpressions;

public class HighscoreManager : MonoBehaviour
{
    public static HighscoreManager instance;

    [SerializeField] GameObject highscorePanel;
    [SerializeField] TextMeshProUGUI[] allUsernameText;
    [SerializeField] TextMeshProUGUI[] allHighscoreTexts;
    [SerializeField] int numberOfAllHighscore;
    [SerializeField] int numberPerPageHighscore;
    List<HighscoreData> allLatestHighscore;
    public int smallestHighscore { get; private set; }

    [SerializeField] Button[] allModeHighscoreButton;
    [SerializeField] Button[] allDeleteModeHighscoreButton;
    [SerializeField] Button nextPageButton;
    [SerializeField] Button previousPageButton;
    int currentPageIndex;
    int currentModeIndex;
    int currentNewHighscoreIndex = -1;
    int currentNewHighscoreModeIndex = -1;

    [SerializeField] GameObject newHighscorePanel;
    [SerializeField] TextMeshProUGUI newHighscoreInfoText;
    [SerializeField] Button newHighscoreProceedButton;
    [SerializeField] TMP_InputField usernameInputField;

    #region UNITY CALLBACKS
    private void Awake()
    {
        if (instance == null) instance = this;
        else if (instance != null && instance != this) { Destroy(gameObject); return; }

        SetupHighscoreVisual();
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.O) && Input.GetKey(KeyCode.P))
        {
            if (Input.GetKeyDown(KeyCode.L))
            {
                UpdateHighscoreVisual(currentModeIndex, currentPageIndex, true);
            }
        }
    }
    #endregion

    #region PRIVATE FUNCTION
    void SetupHighscoreVisual()
    {
        string modeNormalFilename = Keyword.HIGHSCORE_FILENAME + Keyword.PAINT_MODE_NORMAL;
        var modeNormalHighscore = GetCurrentHighscore(modeNormalFilename);
        if (modeNormalHighscore == null) modeNormalHighscore = new HighscoreData(modeNormalFilename, numberOfAllHighscore);

        string modeEndlessFilename = Keyword.HIGHSCORE_FILENAME + Keyword.PAINT_MODE_ENDLESS;
        var modeEndlessHighscore = GetCurrentHighscore(modeEndlessFilename);
        if (modeEndlessHighscore == null) modeEndlessHighscore = new HighscoreData(modeEndlessFilename, numberOfAllHighscore);

        string modeColorchangeFilename = Keyword.HIGHSCORE_FILENAME + Keyword.PAINT_MODE_COLORCHANGE;
        var modeColorchangeHighscore = GetCurrentHighscore(modeColorchangeFilename);
        if (modeColorchangeHighscore == null) modeColorchangeHighscore = new HighscoreData(modeColorchangeFilename, numberOfAllHighscore);

        allLatestHighscore = new List<HighscoreData> { modeNormalHighscore, modeEndlessHighscore,modeColorchangeHighscore };
        highscorePanel.SetActive(false);
        newHighscorePanel.SetActive(false);
    }

    void UpdateHighscoreVisual(int modeIndex, int pageIndex, bool deletable = false)
    {
        var latestHighscore = allLatestHighscore[modeIndex];
        pageIndex = Mathf.Clamp(pageIndex, 0, numberOfAllHighscore / numberPerPageHighscore - 1);
        int firsthHighscoreIndex = numberPerPageHighscore * pageIndex;
        int highlightedHighscoreIndex = currentNewHighscoreIndex - firsthHighscoreIndex;
        for (int i = 0; i < numberPerPageHighscore; i++)
        {
            int highscoreIndex = i + firsthHighscoreIndex;
            PlayerData playerData = latestHighscore.allHighscore[highscoreIndex];
            bool isHighlighting = i == highlightedHighscoreIndex && SceneManager.GetActiveScene().name == Keyword.SCENE_CATCH_THE_PAINT && currentNewHighscoreModeIndex == modeIndex;
            string usernameSentence = isHighlighting ? Keyword.TAG_COLOR_ORANGE : Keyword.TAG_COLOR_BLACK;
            string highscoreSentence = isHighlighting ? Keyword.TAG_COLOR_ORANGE : Keyword.TAG_COLOR_BLACK;
            if (playerData.username == Keyword.NULL)
            {
                usernameSentence += (highscoreIndex + 1).ToString() + ". ";
                highscoreSentence += "";
            }
            else
            {
                usernameSentence += (highscoreIndex + 1).ToString() + ". " + playerData.username;
                highscoreSentence += playerData.score;
            }
            allUsernameText[i].SetText(usernameSentence);
            allHighscoreTexts[i].SetText(highscoreSentence);
        }

        currentModeIndex = modeIndex;
        currentPageIndex = pageIndex;
        UpdateAllModeDeleteHighscoreActivation(deletable);
        UpdateAllModeHighscoreButtonActivation();
        UpdateNavigationButtonActivation();
    }

    void UpdateAllModeHighscoreButtonActivation()
    {
        float xModeClickablePos = 240f;
        float xModeUnclickableePos = 280f;
        float yStartPos = 185;
        float yIntervalValue = -185;

        for (int i=0; i< allModeHighscoreButton.Length; i++)
        {
            bool isSelected = i == currentModeIndex;
            Vector3 selectedModePosition = new Vector3(xModeUnclickableePos, yStartPos + (i * yIntervalValue));
            Vector3 nonselectedModePosition = new Vector3 (xModeClickablePos, yStartPos + (i * yIntervalValue));
            allModeHighscoreButton[i].transform.localPosition = isSelected? selectedModePosition : nonselectedModePosition;
            allModeHighscoreButton[i].interactable = isSelected ? false : true;
            allModeHighscoreButton[i].transform.GetChild(0).gameObject.SetActive(isSelected);

            selectedModePosition.x += 75;
            nonselectedModePosition.x += 75;
            allDeleteModeHighscoreButton[i].transform.localPosition = isSelected ? selectedModePosition : nonselectedModePosition;
        }
    }

    void UpdateAllModeDeleteHighscoreActivation(bool deletable)
    {
        for (int i = 0; i < allDeleteModeHighscoreButton.Length; i++)
            allDeleteModeHighscoreButton[i].gameObject.SetActive(deletable);
    }

    void UpdateNavigationButtonActivation()
    {
        if (currentPageIndex <= 0)
        {
            previousPageButton.interactable = false;
            nextPageButton.interactable = true;
        }
        else if (currentPageIndex >= numberOfAllHighscore / numberPerPageHighscore - 1)
        {
            nextPageButton.interactable = false;
            previousPageButton.interactable = true;
        }
        else
        {
            previousPageButton.interactable = true;
            nextPageButton.interactable = true;
        }
    }

    HighscoreData GetCurrentHighscore(string filename)
    {
        return HighscoreDataSerializer.Load(filename);
    }

    void SetCurrentHighscore(PlayerData playerData)
    {
        int modeIndex = GameManager.instance.GetGameplayData().mode;
        var currentHighscore = allLatestHighscore[modeIndex].allHighscore;

        int indexToRemove = 0;
        int indexToInsert = 0;
        int smallestHighscore = int.MaxValue;
        bool isRemovingNull = false;

        for (int i = 0; i < numberOfAllHighscore; i++)
        {
            if (currentHighscore == null || currentHighscore[i].username == Keyword.NULL)
            {
                if (smallestHighscore >= playerData.score && !isRemovingNull) { indexToInsert = i; isRemovingNull = true; }
                indexToRemove = i;
                smallestHighscore = smallestHighscore > 100 ? 100 : smallestHighscore;
            }
            else if (currentHighscore[i].score < smallestHighscore)
            {
                if (smallestHighscore >= playerData.score) indexToInsert = i;
                indexToRemove = i;
                smallestHighscore = currentHighscore[i].score;
            }
            else if (currentHighscore[i].score == smallestHighscore)
            {
                indexToRemove = i;
            }
        }

        currentHighscore.RemoveAt(indexToRemove);
        currentHighscore.Insert(indexToInsert, playerData);

        currentNewHighscoreIndex = indexToInsert;
        currentNewHighscoreModeIndex = modeIndex;
        allLatestHighscore[modeIndex].SetAndSaveHighscore(currentHighscore);
        UpdateHighscoreVisual(modeIndex, indexToInsert / numberPerPageHighscore);
    }

    bool IsValidUsername(string username)
    {
        string invalidMessege = Keyword.NULL;

        if (username.Length <= 0) invalidMessege = Keyword.HIGHSCORE_INPUTNAME_INVALID_EMPTY;
        else if (username[0] == ' ') invalidMessege = Keyword.HIGHSCORE_INPUTNAME_INVALID_SPACEATFIRST;
        else if (!Regex.IsMatch(username, @"[a-zA-Z]")) invalidMessege = Keyword.HIGHSCORE_INPUTNAME_INVALID_NONALPHABET;
        else if (username.Length < 3) invalidMessege = Keyword.HIGHSCORE_INPUTNAME_INVALID_TOOSHORT;
        else if (name == Keyword.NULL) invalidMessege = Keyword.HIGHSCORE_INPUTNAME_INVALID_NULL;

        if (invalidMessege != Keyword.NULL) { newHighscoreInfoText.SetText(invalidMessege); newHighscoreInfoText.color = Color.red; }
        return invalidMessege == Keyword.NULL;
    }
    #endregion

    #region PUBLIC FUNCTION
    public void OpenNewHighscorePanel()
    {
        usernameInputField.text = "";
        newHighscoreInfoText.SetText(Keyword.HIGHSCORE_INPUTNAME_INSTRUCTION);
        newHighscoreInfoText.color = Color.black;
        newHighscoreProceedButton.interactable = true;
        newHighscorePanel.SetActive(true);
        VirtualKeyboardManager.instance.OpenKeyboard(usernameInputField);
    }

    public void OpenHighscorePanel()
    {
        UpdateHighscoreVisual(0,0);
        highscorePanel.SetActive(true);
    }

    public void CloseHighscorePanel()
    {
        highscorePanel.SetActive(false);
    }

    public void SetSmallestHighscore()
    {
        smallestHighscore = allLatestHighscore[GameManager.instance.GetGameplayData().mode].allHighscore[numberOfAllHighscore - 1].score;
    }
    #endregion

    #region BUTTON & LISTENER
    public void ProceedNewHighscoreButton()
    {
        if (!IsValidUsername(usernameInputField.text)) return;

        newHighscoreProceedButton.interactable = false;
        VirtualKeyboardManager.instance.CloseKeyboard();
        SetCurrentHighscore(new PlayerData(usernameInputField.text, GameManager.instance.GetGameplayData().score));
        newHighscorePanel.SetActive(false);
        highscorePanel.SetActive(true);
        AudioManager.instance.PlayButtonAudio();
    }

    public void CloseButton()
    {
        if (SceneManager.GetActiveScene().name == Keyword.SCENE_MAIN_MENU) CloseHighscorePanel();
        else GameManager.instance.LoadScene(Keyword.SCENE_MAIN_MENU);
        AudioManager.instance.PlayButtonAudio();
    }

    public void ChangeModeHighscoreButton(int modeIndex)
    {
        UpdateHighscoreVisual(modeIndex, 0);
        AudioManager.instance.PlayButtonAudio();
    }

    public void DeleteModeHighscoreButton(int modeIndex)
    {
        allLatestHighscore[modeIndex].ResetAndSaveHighscore();
        UpdateHighscoreVisual(currentModeIndex, currentPageIndex);
        AudioManager.instance.PlayButtonAudio();
    }

    public void NextPageButton()
    {
        UpdateHighscoreVisual(currentModeIndex,currentPageIndex + 1);
        AudioManager.instance.PlayButtonAudio();
    }

    public void PreviousPageButton()
    {
        UpdateHighscoreVisual(currentModeIndex,currentPageIndex - 1);
        AudioManager.instance.PlayButtonAudio();
    }
    #endregion
}

#region DATA CLASS
public class HighscoreDataSerializer
{
    public static HighscoreData Load(string filename)
    {
        string directory = Application.persistentDataPath + "/" + Keyword.HIGHSCORE_DIRECTORY;
        filename = directory + "/" + filename;
        if (File.Exists(filename))
        {
            try
            {
                using (Stream stream = File.OpenRead(filename))
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    return formatter.Deserialize(stream) as HighscoreData;
                }
            }
            catch (Exception e)
            {
                Debug.Log(e.Message);
            }
        }
        return null;
    }

    public static void Save(string filename, HighscoreData data) 
    {
        string directory = Application.persistentDataPath + "/" + Keyword.HIGHSCORE_DIRECTORY;
        if (!Directory.Exists(directory)) Directory.CreateDirectory(directory);

        filename = directory + "/" + filename;
        using (Stream stream = File.OpenWrite(filename)) 
        {
            BinaryFormatter formatter = new BinaryFormatter();
            formatter.Serialize(stream, data);
        }
        
    }

    public static void Delete(string filename)
    {
        string directory = Application.persistentDataPath + "/" + Keyword.HIGHSCORE_DIRECTORY;
        filename = directory + "/" + filename;

        if(Directory.Exists(directory)) File.Delete(filename);
    }
}

[Serializable] public class HighscoreData
{
    public List<PlayerData> allHighscore { get; private set; }
    public string filename { get; private set; }
    public int totalNumber { get; private set; }

    public HighscoreData(string _filename, int _totalNumber = 100)
    {
        filename = _filename;
        totalNumber = _totalNumber;
        ResetData();
    }

    void ResetData()
    { 
        allHighscore = new List<PlayerData>();
        for (int i = 0; i < totalNumber; i++) allHighscore.Add(new PlayerData());
    }

    public void SetAndSaveHighscore(List<PlayerData> newData)
    {
        allHighscore = new List<PlayerData>(newData);
        HighscoreDataSerializer.Save(filename, MemberwiseClone() as HighscoreData);
    }

    public void ResetAndSaveHighscore()
    {
        ResetData();
        HighscoreDataSerializer.Delete(filename);
    }
}

[Serializable] public class PlayerData
{
    public string username { get; private set; }
    public int score { get; private set; }

    public PlayerData(string username, int score)
    {
        this.username = username;
        this.score = score;
    }

    public PlayerData()
    {
        username = Keyword.NULL;
        score = -1;
    }

}
#endregion